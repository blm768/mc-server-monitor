function statusIsLoaded(doc) {
    if (doc === null)
        return false;
    if (doc.readyState != 'complete')
        return false;
    // Workaround for Chromium bug 936331; readyState is unreliable for iframes.
    if (doc.body.children.length == 0)
        return false;
    return true;
}

function initStatus() {
    let statusSection = document.getElementById('server-status');
    let statusPage = document.getElementById('status-data');
    statusSection.classList.add('loading');
    let onLoad = () => { statusSection.classList.remove('loading'); };
    if (!statusIsLoaded(statusPage.contentDocument)) {
        statusPage.addEventListener('load', onLoad);
    } else {
        onLoad();
    }
}

if (document.readyState == 'loading') {
    document.addEventListener('DOMContentLoaded', initStatus);
} else {
    initStatus();
}
