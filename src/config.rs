use std::net::{IpAddr, Ipv4Addr, Ipv6Addr, SocketAddr};
use std::path::PathBuf;

use macaddr::MacAddr;
use serde::{Deserialize, Deserializer, Serialize, Serializer};

#[derive(Debug, Deserialize, Serialize)]
pub struct Config {
    #[serde(default = "default_listen_addrs")]
    pub listen_addrs: Vec<SocketAddr>,

    #[serde(
        deserialize_with = "mac_addr_from_string",
        serialize_with = "mac_addr_to_string"
    )]
    pub wake_mac: MacAddr,
    pub wake_ip: IpAddr,
    pub wake_scope: Option<u32>, // TODO: support textual scope IDs on platforms that can use them. (Should also support in-place parsing of scope identifiers)
    #[serde(default = "default_wake_port")]
    pub wake_port: u16,

    pub server_addr: SocketAddr,
    pub server_addr_scope: Option<u32>,

    #[serde(default = "default_asset_path")]
    pub asset_path: PathBuf,
}

impl Config {
    pub fn wake_broadcast_addr(&self) -> SocketAddr {
        let mut sock_addr = SocketAddr::new(self.wake_ip, self.wake_port);
        if let Some(scope) = self.wake_scope {
            if let SocketAddr::V6(ref mut addr) = sock_addr {
                addr.set_scope_id(scope);
            }
        }
        sock_addr
    }

    pub fn server_addr(&self) -> SocketAddr {
        let mut sock_addr = self.server_addr;
        if let Some(scope) = self.server_addr_scope {
            if let SocketAddr::V6(ref mut addr) = sock_addr {
                addr.set_scope_id(scope);
            }
        }
        sock_addr
    }
}

const DEFAULT_LISTEN_PORT: u16 = 8080;

fn default_listen_addrs() -> Vec<SocketAddr> {
    vec![
        SocketAddr::new(Ipv4Addr::new(127, 0, 0, 1).into(), DEFAULT_LISTEN_PORT),
        SocketAddr::new(
            Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 1).into(),
            DEFAULT_LISTEN_PORT,
        ),
    ]
}

const fn default_wake_port() -> u16 {
    9 // Discard protocol (commonly recommended for wake-on-LAN)
}

#[cfg(all(feature = "deploy", target_family = "unix"))]
const DEFAULT_ASSETS_PATH: &str = "/usr/share/mc-server-monitor";
#[cfg(all(feature = "deploy", target_family = "windows"))]
const DEFAULT_ASSETS_PATH: &str = "./assets";
#[cfg(not(feature = "deploy"))]
const DEFAULT_ASSETS_PATH: &str = "./assets";

fn default_asset_path() -> PathBuf {
    PathBuf::from(DEFAULT_ASSETS_PATH)
}

fn mac_addr_to_string<S: Serializer>(addr: &MacAddr, serializer: S) -> Result<S::Ok, S::Error> {
    serializer.serialize_str(
        &addr
            .as_bytes()
            .iter()
            .map(|b| format!("{:02x}", b))
            .collect::<Vec<_>>()
            .join(":"),
    )
}

fn mac_addr_from_string<'de, D: Deserializer<'de>>(deserializer: D) -> Result<MacAddr, D::Error> {
    struct MacAddrDeserializer;

    impl serde::de::Visitor<'_> for MacAddrDeserializer {
        type Value = MacAddr;
        fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
            formatter.write_str("a string containing a MAC address")
        }
        fn visit_str<E: serde::de::Error>(self, s: &str) -> Result<Self::Value, E> {
            s.parse()
                .map_err(|e| serde::de::Error::custom(format!("invalid MAC address: {}", e)))
        }
    }

    deserializer.deserialize_str(MacAddrDeserializer)
}
