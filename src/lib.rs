#![feature(proc_macro_hygiene)]

pub mod config;
pub mod status;
pub mod templates;
pub mod wake;
