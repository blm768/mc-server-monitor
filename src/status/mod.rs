use serde::{Deserialize, Serialize};

pub mod net;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Status {
    pub players: Players,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Players {
    pub max: usize,
    pub online: usize,
}
