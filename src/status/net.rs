use std::convert::TryInto;
use std::future::Future;
use std::net::SocketAddr;
use std::pin::Pin;

use bytes::{BufMut, Bytes, BytesMut};
use pin_utils::pin_mut;
use tokio::io::{AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt, BufReader, ErrorKind};
use tokio::net::TcpStream;

use crate::status::Status;

// TODO: tests would be good.

pub async fn get_status(addr: SocketAddr) -> tokio::io::Result<Status> {
    // TODO: boxing because we otherwise overflow the type length limit. May be an upstream tokio issue.
    let box_connect: Pin<Box<dyn Future<Output = _> + Send>> = Box::pin(TcpStream::connect(addr));
    let mut stream = box_connect.await?;
    let (read, write) = stream.split();
    let read = BufReader::new(read);
    pin_mut!(read);
    pin_mut!(write);

    send_handshake(write.as_mut(), &addr).await?;
    send_packet(write, REQUEST_ID, &[]).await?;
    let status = recv_status(read).await?;

    Ok(status)
}

fn data_error<S: Into<String>>(msg: S) -> tokio::io::Error {
    tokio::io::Error::new(ErrorKind::InvalidData, msg.into())
}

const PROTOCOL_VERSION: i32 = 4;

const HANDSHAKE_ID: i32 = 0;
const REQUEST_ID: i32 = 0;
const STATUS_RESPONSE_ID: i32 = 0;

const fn max_varint_bytes(size: usize) -> usize {
    let bits = size * 8;
    bits / 7 + ((bits % 7 != 0) as usize)
}

const MAX_VARINT_BYTES: usize = max_varint_bytes(std::mem::size_of::<i32>());

#[derive(Debug, Clone, Copy)]
struct VarInt {
    len: usize,
    bytes: [u8; MAX_VARINT_BYTES],
}

impl VarInt {
    pub fn len(&self) -> usize {
        self.len
    }

    pub fn bytes(&self) -> &[u8] {
        &self.bytes[0..self.len]
    }
}

impl From<i32> for VarInt {
    fn from(value: i32) -> Self {
        let mut value = value as u32; // Want logical shifting semantics
        let mut len = 0;
        let mut bytes: [u8; MAX_VARINT_BYTES] = [0; MAX_VARINT_BYTES];
        loop {
            let byte = (value & 0b0111_1111) as u8;
            value >>= 7;
            let done = value == 0;
            if !done {
                value |= 0b1000_0000;
            }
            bytes[len] = byte;
            len += 1;
            if done {
                break;
            }
        }
        Self { len, bytes }
    }
}

fn varint_size(size: usize) -> tokio::io::Result<VarInt> {
    let size: i32 = size
        .try_into()
        .map_err(|_| data_error("packet data too long"))?;
    Ok(VarInt::from(size))
}

fn send_str<E: Extend<u8>>(buf: &mut E, text: &str) -> tokio::io::Result<()> {
    let len = varint_size(text.len())?;
    buf.extend(len.bytes().iter().copied());
    buf.extend(text.bytes());
    Ok(())
}

async fn send_packet(
    mut stream: Pin<&mut (dyn AsyncWrite + Send)>,
    id: i32,
    data: &[u8],
) -> tokio::io::Result<()> {
    let id = VarInt::from(id);
    let len = varint_size(id.len() + data.len())?;
    stream.write_all(len.bytes()).await?;
    stream.write_all(id.bytes()).await?;
    stream.write_all(data).await?;
    Ok(())
}

async fn send_handshake(
    write: Pin<&mut (dyn AsyncWrite + Send)>,
    server_addr: &SocketAddr,
) -> tokio::io::Result<()> {
    let mut buf = BytesMut::new(); // TODO: reserve space.

    let version = VarInt::from(PROTOCOL_VERSION);
    buf.put(version.bytes());

    let remote_addr = format!("{}", server_addr.ip());
    send_str(&mut buf, &remote_addr)?;
    buf.put_u16(server_addr.port());

    let next_state = VarInt::from(1);
    buf.put(next_state.bytes());

    send_packet(write, HANDSHAKE_ID, &buf).await
}

async fn recv_varint(
    mut read: Pin<&mut (dyn AsyncRead + Send)>,
) -> Result<(i32, usize), tokio::io::Error> {
    let mut value = 0;
    let mut len = 0;
    loop {
        let byte = read.read_u8().await?;
        value |= ((byte & 0b0111_1111) as u32) << (len * 7);
        len += 1;
        if byte & 0b1000_0000 == 0 {
            break;
        }
        if len == MAX_VARINT_BYTES {
            return Err(data_error("VarInt too long"));
        }
    }
    Ok((value as i32, len))
}

async fn recv_len(
    read: Pin<&mut (dyn AsyncRead + Send)>,
) -> Result<(usize, usize), tokio::io::Error> {
    let (size, len) = recv_varint(read).await?;
    let size: usize = size
        .try_into()
        .map_err(|_| data_error("invalid (negative) data length"))?;
    Ok((size, len))
}

async fn recv_str_bytes(
    mut read: Pin<&mut (dyn AsyncRead + Send)>,
) -> Result<Bytes, tokio::io::Error> {
    let (len, _) = recv_len(read.as_mut()).await?;
    let mut buf = BytesMut::with_capacity(len);
    unsafe { buf.set_len(len) };
    read.read_exact(&mut buf).await?;
    Ok(buf.freeze())
}

#[derive(Clone, Debug)]
struct PacketHeader {
    pub _data_len: usize,
    pub id: i32,
}

async fn recv_packet_header(
    mut read: Pin<&mut (dyn AsyncRead + Send)>,
) -> tokio::io::Result<PacketHeader> {
    let (len, _) = recv_len(read.as_mut()).await?;

    let (id, id_len) = recv_varint(read.as_mut()).await?;
    let data_len = len
        .checked_sub(id_len)
        .ok_or_else(|| data_error("packet length was incorrect"))?;

    Ok(PacketHeader {
        _data_len: data_len,
        id,
    })
}

async fn recv_status(mut read: Pin<&mut (dyn AsyncRead + Send)>) -> tokio::io::Result<Status> {
    let header = recv_packet_header(read.as_mut()).await?;
    if header.id != STATUS_RESPONSE_ID {
        return Err(data_error("invalid response ID"));
    }
    // TODO: make sure JSON length (including string length field) matches packet length?

    let json = recv_str_bytes(read).await?;
    serde_json::from_slice(&json).map_err(|e| data_error(format!("failed to parse status: {}", e)))
}
