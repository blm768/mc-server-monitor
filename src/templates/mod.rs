use maud::{html, Markup};

use crate::status::Status;

pub fn index() -> Markup {
    html! {
        html {
            head {
                link rel="stylesheet" type="text/css" href="/css/index.css" {}
                script type="text/javascript" src="/js/index.js" {}
            }
            body {
                section id="server-status" {
                    iframe id="status-data" src="/status" {}
                }
                form action="/wake" method="POST" {
                    input id="wake-server" type="submit" value="Wake server" {}
                }
            }
        }
    }
}

pub fn status(status: Option<&Status>) -> Markup {
    let plural_s = |n| match n {
        1 => "",
        _ => "s",
    };

    html! {
        html {
            body {
                @match status {
                    Some(status) => (status.players.online) " player" (plural_s(status.players.online)) " online",
                    None => "Server offline",
                }
            }
        }
    }
}
