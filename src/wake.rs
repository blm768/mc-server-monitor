use std::net::{IpAddr, Ipv4Addr, Ipv6Addr, SocketAddr};

use macaddr::MacAddr;
use tokio::net::UdpSocket;

pub async fn wake(mac_addr: MacAddr, broadcast_addr: SocketAddr) -> Result<(), tokio::io::Error> {
    let bind_addr = bind_any_address(&broadcast_addr);
    let sock = UdpSocket::bind((bind_addr, 0)).await?;
    sock.set_broadcast(true)?;
    let message: Vec<u8> = wake_message(&mac_addr);
    let mut buf = &message[..];
    while !buf.is_empty() {
        let sent = sock.send_to(buf, broadcast_addr).await?;
        buf = &buf[sent..];
    }
    Ok(())
}

/// Arguments
///
/// * `base_addr` - used to determine whether to return an IPv4 or IPv6 address.
fn bind_any_address(base_addr: &SocketAddr) -> IpAddr {
    match base_addr {
        SocketAddr::V4(_) => Ipv4Addr::new(0, 0, 0, 0).into(),
        SocketAddr::V6(_) => Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 0).into(),
    }
}

// TODO: probably can't handle EUI-64 addresses
pub fn wake_message(mac_addr: &MacAddr) -> Vec<u8> {
    std::iter::repeat(255u8)
        .take(6)
        .chain(
            std::iter::repeat(mac_addr.as_bytes())
                .take(16)
                .flat_map(<[u8]>::iter)
                .copied(),
        )
        .collect()
}
