use std::net::SocketAddr;
use std::path::{Path, PathBuf};

use clap::Parser;
use futures::stream::StreamExt;
use log::error;
use macaddr::MacAddr;
use maud::Markup;
use warp::filters::fs::File;
use warp::http::Uri;
use warp::{Filter, Rejection};

use mc_server_monitor::config::Config;
use mc_server_monitor::status;
use mc_server_monitor::templates;
use mc_server_monitor::wake;

// TODO: better error type
fn load_config(path: &Path) -> Result<Config, ()> {
    let config_file = std::fs::read_to_string(path);
    match config_file {
        Ok(text) => match toml::from_str(&text) {
            Ok(config) => return Ok(config),
            Err(e) => error!("Error parsing configuration file: {}", e),
        },
        Err(e) => error!("Unable to load config file: {}", e),
    }
    Err(())
}

fn asset_dir(
    path: &'static str,
    dir: impl Into<PathBuf>,
) -> impl Filter<Extract = (File,), Error = Rejection> + Clone {
    warp::path(path).and(warp::get()).and(warp::fs::dir(dir))
}

fn html_page(markup: Markup) -> impl warp::Reply {
    warp::reply::html(markup.into_string())
}

async fn render_index() -> Result<impl warp::Reply, Rejection> {
    Ok(html_page(templates::index()))
}

async fn render_status(server_addr: SocketAddr) -> Result<impl warp::Reply, Rejection> {
    // TODO: detect errors that aren't "expected".
    let status = status::net::get_status(server_addr)
        .await
        .map_err(|e| log::error!("{}", e))
        .ok();
    Ok(html_page(templates::status(status.as_ref())))
}

#[derive(Debug, thiserror::Error)]
#[error("failed to wake server")]
struct WakeError(#[source] tokio::io::Error);

impl warp::reject::Reject for WakeError {}

async fn do_wake(
    mac_addr: MacAddr,
    broadcast_addr: SocketAddr,
) -> Result<impl warp::Reply, Rejection> {
    match wake::wake(mac_addr, broadcast_addr).await {
        Ok(()) => {
            let redirect = warp::redirect::redirect(Uri::from_static("/"));
            Ok(warp::reply::with_status(
                redirect,
                warp::http::StatusCode::SEE_OTHER,
            ))
        }
        Err(e) => Err(warp::reject::custom(WakeError(e))),
    }
}

fn handle_stream_error(e: tokio::io::Error) {
    error!("Failed to make TCP connection: {}", e);
}

#[derive(Debug, Parser)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    config: Option<String>,
}

#[tokio::main]
async fn main() -> Result<(), ()> {
    env_logger::init();

    let args = Args::parse();
    let config_path = args.config.unwrap_or("config.toml".to_string());

    let config = load_config(Path::new(&config_path))?;

    // Boxing to keep from hitting the type length limit
    let asset_path = &config.asset_path;
    let assets = Filter::boxed(
        asset_dir("images", asset_path.join("images"))
            .or(asset_dir("css", asset_path.join("css")))
            .or(asset_dir("js", asset_path.join("js"))),
    );

    let server_addr = config.server_addr();
    let index = warp::path::end().and(warp::get()).and_then(render_index);
    let status = warp::path("status")
        .and(warp::path::end())
        .and(warp::get())
        .and_then(move || render_status(server_addr));
    let wake_mac = config.wake_mac;
    let wake_broadcast = config.wake_broadcast_addr();
    let wake = warp::path("wake")
        .and(warp::post())
        .and_then(move || do_wake(wake_mac, wake_broadcast)); // TODO: better rejection handling

    let routes = assets.or(index).or(status).or(wake);

    let mut sockets: Vec<_> = futures::stream::iter(config.listen_addrs.into_iter())
        .then(tokio::net::TcpListener::bind)
        .filter_map(|r| async { r.map_err(handle_stream_error).ok() })
        .collect()
        .await;
    let connection_streams = sockets.iter_mut().map(|s| {
        futures::stream::poll_fn(move |cx| {
            s.poll_accept(cx).map_ok(|(stream, _addr)| stream).map(Some)
        })
    });
    let connections = futures::stream::select_all(connection_streams);
    warp::serve(routes).run_incoming(connections).await;
    Ok(())
}
