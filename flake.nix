{
  description = "A tool to monitor and remotely wake a Minecraft server";
  inputs = {
    flake-parts.url = "github:hercules-ci/flake-parts";
    nixpkgs.follows = "nixCargoIntegration/nixpkgs";
    nixCargoIntegration.url = "github:yusdacra/nix-cargo-integration";
  };
  outputs = inputs:
    inputs.flake-parts.lib.mkFlake { inherit inputs; } ({ moduleWithSystem, ... }: {
      imports = [
        inputs.flake-parts.flakeModules.easyOverlay
        inputs.nixCargoIntegration.flakeModule
      ];
      systems = [ "x86_64-linux" ];
      perSystem = { config, lib, pkgs, system, ... }: {
        devShells.default = config.nci.outputs.mc-server-monitor.devShell.overrideAttrs (old: {
          packages = (old.packages or [ ]) ++ [ pkgs.cargo-outdated pkgs.rust-analyzer ];
        });
        nci.projects.mc-server-monitor.path = ./.;
        nci.crates.mc-server-monitor = {
          export = true;
        };
        packages.default = config.nci.outputs.mc-server-monitor.packages.release;
        overlayAttrs = {
          mc-server-monitor = config.packages.mc-server-monitor-release;
        };
      };
      flake = {
        nixosModules.mc-server-monitor = moduleWithSystem (perSystem@{ config }: { config, lib, pkgs, ... }:
          with lib;
          let
            cfg = config.services.mc-server-monitor;
          in
          {
            options.services.mc-server-monitor = {
              enable = mkOption {
                type = types.bool;
                default = false;
                description = "Enables the service";
              };
              listen.address = mkOption {
                type = types.str;
                default = "0.0.0.0:80";
                description = "The address to which the Web server will bind";
              };
              server.address = mkOption {
                type = types.str;
                example = "192.168.0.3:25565";
                description = "IP address and port of the Minecraft server";
              };
              wake = {
                ip = mkOption {
                  type = types.str;
                  description = "Destination IP address for Wake-on-LAN packets";
                };
                mac = mkOption {
                  type = types.str;
                  description = "Destination MAC address for Wake-on-LAN packets";
                };
              };
            };
            config = mkIf cfg.enable {
              environment.etc."mc-server-monitor/config.toml".text = ''
                listen_addrs = ["${cfg.listen.address}"]
                server_addr = "${cfg.server.address}"
                wake_ip = "${cfg.wake.ip}"
                wake_mac = "${cfg.wake.mac}"
                asset_path = "${./assets}"
              '';
              systemd.services.mc-server-monitor = {
                after = [ "network-online.target" ];
                wants = [ "network-online.target" ];
                serviceConfig = {
                  Type = "simple";
                  ExecStart = "${perSystem.config.packages.default}/bin/mc-server-monitor -c /etc/mc-server-monitor/config.toml";
                };
                wantedBy = [ "multi-user.target" ];
                enable = true;
              };
            };
          });
      };
    });
}
